package com.example.demoapi.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @NotEmpty(message = "Обязательно для заполнения")
    @Size(min = 1, max = 255, message = "User name must be a string")
    @Pattern(regexp = "\\w+", message = "User name must contain only letters, digits, and underscores")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private String userName;

    @NotEmpty(message = "Обязательно для заполнения")
    @Email(message = "Invalid email format")
    private String email;
}
