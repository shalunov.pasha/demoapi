package com.example.demoapi.controllers;

import com.example.demoapi.models.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class NewController {

    @PostMapping("/name")
    public ResponseEntity<Map<String, Object>> addName(@Valid @RequestBody User user) {
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("user", user);
        responseBody.put("status", "ok");

        return ResponseEntity.ok(responseBody);
    }
}
